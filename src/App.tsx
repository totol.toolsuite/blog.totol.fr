import { Link, NavLink, RouterProvider, createBrowserRouter } from "react-router-dom"
import Single from './pages/Single'
const router = createBrowserRouter(
  [
    {
      path: '/',
      element: <div>    
        <nav>
          <NavLink to='/'>accueil</NavLink>
          <Link to='/blog'>blog</Link>
        </nav>
        Page d'accueil
      </div>
    },
    {
      path: '/blog',
      element: <div>Mon blog</div>
    },
    {
      path: '/blog/:id',
      element: <Single></Single>
    }
  ]
)

function App() {
  return <>

    <RouterProvider router={router}></RouterProvider>
  </>
}

export default App

