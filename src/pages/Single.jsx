import { useParams } from "react-router-dom";

export default function Single () {
  const { id } = useParams()

  return <div>
    Article {id}
  </div>
}