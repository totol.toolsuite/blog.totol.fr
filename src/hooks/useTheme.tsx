import { createContext, useContext, useState } from "react";

const ThemeContext = createContext('light')

export function useTheme() {
  const theme = useContext(ThemeContext)
  return {
    isLight: theme === 'light',
    isDark: theme === 'dark',
    theme
  }
}

export default function ThemeContextProvider ({children}) {
  const [theme, setTheme] = useState('light')

  const handleToggleTheme = () => {
    if (theme === 'light') setTheme('dark')
    else setTheme('light')
  }

  return <ThemeContext.Provider value={theme}>
    <button onClick={handleToggleTheme}>switch theme</button>
    {children}
  </ThemeContext.Provider>
}