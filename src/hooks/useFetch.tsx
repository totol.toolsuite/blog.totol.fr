import { useEffect, useState} from "react";

export function useFetch(url: string, options = {}) {
  const [loading, setLoading] = useState(true)
  const [errors, setErrors] = useState(null)
  const [data, setData] = useState()

  const fetchData = async () => {
    try {
      const data = await (await fetch(url,
        {
          ...options,
          headers: {
            'Accept': 'application/json; charset=UTF-8',
            ...options.headers
          }
        })).json()
      setData(data)
    } catch (e) {
      setErrors(e as any)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return {
    loading,
    errors,
    data
  }
}