import { useEffect } from "react"

export function useDocumentTitle(newTitle = 'default title') {
  useEffect(() => {
    document.title = newTitle
  }, [newTitle])
}