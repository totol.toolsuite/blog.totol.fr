import {describe, it, expect} from 'vitest'
import { useIncrement } from '../src/hooks/useIncrement'
import { act, renderHook } from '@testing-library/react'

describe('useIncrement', () => {
  it('should use the default value', () => {
    const { result } = renderHook(() => useIncrement(5))
    expect(result.current.state).toBe(5)
  })
  it('should increment', () => {
    const { result } = renderHook(() => useIncrement(5))
    act(() => result.current.increment())
    expect(result.current.state).toBe(6)
  })
})