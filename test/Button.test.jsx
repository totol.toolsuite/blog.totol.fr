import {describe, it, expect} from 'vitest'
import { Button } from '../src/App'
import { render, screen } from '@testing-library/react'


describe('<Button></Button>', () => {
  it('should render the button', () => {
    const { container } = render(<Button type="danger">Erreur</Button>)
    expect(container).toMatchInlineSnapshot(`
      <div>
        <button
          style="color: red;"
        >
          Erreur
        </button>
      </div>
    `)
  })
})